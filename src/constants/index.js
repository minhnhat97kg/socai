import { StyleSheet, Dimensions } from 'react-native'

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width

const window = { width: W, height: H }

const style = {
	RADIUS: 10,
	H1: { fontSize: 26, fontWeight: "bold" },
	H2: { fontSize: 24, fontWeight: "bold" },
	H3: { fontSize: 22, fontWeight: "bold" },
	H4: { fontSize: 20, fontWeight: "bold" },
	P: { fontSize: 14 },
}

const colors = {
	main: "#006991",
	sub: "#5EBF9D",
	lightGrey: "#F5F5F5",
	danger: "#F28A4B",
	warning: "#F8CC61",
	success: '#028153',
	other: "#649ACB"
}
const icons = {
	CUSTOMER: require("../../assets/customer.png"),
	SETTING: require("../../assets/settings.png"),
	SHOPPING: require("../../assets/shopping.png"),
	BOX: require("../../assets/box.png"),
	ANALYTICS: require("../../assets/analytics.png"),
	WAREHOUSE: require("../../assets/warehouse.png"),
	BOOK: require("../../assets/book.png")
};

export default {
	window,
	style,
	icons,
	colors
}