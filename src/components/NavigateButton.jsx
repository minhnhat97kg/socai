import React from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import constants from "../constants";

export default navigateButton = ({
  title,
  containerStyle,
  color,
  titleStyle,
  onPress
}) => {
  return (
    <TouchableOpacity style={[styles(color).Container, containerStyle]} onPress={onPress}>
      <Text style={[styles(color).Title, titleStyle]}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = (color) => {
  let background = {
    danger: constants.colors.danger,
    warning: constants.colors.warning,
    success: constants.colors.success,
  };
  let text = {
    danger: "black",
    warning: "black",
    success: "white",
  };
  return StyleSheet.create({
    Container: {
      alignSelf: "center",
      alignItems: "center",
      justifyContent: "center",
      height: 50,
      backgroundColor: background[color] || "#006991",
      borderRadius: 20,
    },
    Title: {
      fontSize: 20,
      color: text[color] || "white",
      fontWeight: "500",
    },
  });
};
