import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import constant from "../constants";
function Header({
  leftComponent,
  rightComponent,
  children,
  title,
  titleStyle,
}) {
  return (
    <LinearGradient
      style={styles.HeaderContainer}
      colors={["white", "white"]}
      start={{ x: 1, y: 0 }}
      end={{ x: 1.2, y: 2 }}
    >
      <View style={styles.ComponentContainer}>
        {leftComponent}
        <Text style={titleStyle}>{title}</Text>
        {rightComponent}
      </View>
      {children}
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  HeaderContainer: {
    justifyContent: "flex-start",
  },
  ComponentContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
  },
});
export default Header;
