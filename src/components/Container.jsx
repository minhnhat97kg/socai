import React from "react";
import { View, SafeAreaView, Platform } from "react-native";

const Container = (props) => {
  return (
    <SafeAreaView
      style={[
        { flex: 1, paddingTop: 40, backgroundColor: "white" },
        props.style,
      ]}
    >
      {props.children}
    </SafeAreaView>
  );
};
export default Container;
