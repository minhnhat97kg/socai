import React from "react";
import { Text, View, StyleSheet, TextInput } from "react-native";

export default input = ({ title, error, ...rest }) => {
  return (
    <View style={styles.Container}>
      <Text style={styles.Title}>{title}</Text>
      <TextInput style={styles.Input} {...rest} />
      {error && (
        <TextInput style={styles.ErrorText} multiline={true}>
          {error}
        </TextInput>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    justifyContent: "center",
    marginBottom: 20,
  },
  Title: {
    position: "absolute",
    left: 30,
    top: 2,
    fontSize: 18,
    fontWeight: "500",
    backgroundColor: "white",
    color: "#525F65",
    paddingHorizontal: 10,
    marginTop: 0,
    zIndex: 1,
  },
  Input: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#B2B2B2",
    fontSize: 20,
    marginHorizontal: 20,
    marginTop: 15,
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },
  ErrorText: {
    fontSize: 20,
    color: "red",
    marginHorizontal: 20,
  },
});
