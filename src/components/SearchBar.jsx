import React, { useState, useEffect, useMemo } from "react";
import { View, TextInput, TouchableOpacity, StyleSheet } from "react-native";
import { SearchBar } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome5";

function Search({ containerStyle, inputStyle, ...rest }) {
  return (
    <SearchBar
      placeholder="Type Here..."
      containerStyle={styles.containerStyle}
      inputContainerStyle={styles.inputStyle}
      inputStyle={{ fontSize: 13 }}
			{...rest}
      lightTheme
      round
    />
  );
}
export default Search;

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: "white",
    borderWidth: 0, //no effect
    shadowColor: "white", //no effect
    borderBottomColor: "transparent",
    borderTopColor: "transparent",
    height: undefined,
    margin:5,
  },
  inputStyle: {
    height: 20,
  },
});
