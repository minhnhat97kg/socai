import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
//import screens
import Home from "../screens/Home";
import CreateVendor from "../screens/Vendor/Create";
import UpdateVendor from "../screens/Vendor/Update";
import CreateCustomer from "../screens/Customer/Create";
import UpdateCustomer from "../screens/Customer/Update";
import Debt from "../screens/Debt";
import Vendor from "../screens/Vendor";
import Customer from "../screens/Customer";
//constants
const Stack = createNativeStackNavigator();

const Navigator = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Vendor"
        component={Vendor}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CreateVendor"
        component={CreateVendor}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UpdateVendor"
        component={UpdateVendor}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Customer"
        component={Customer}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CreateCustomer"
        component={CreateCustomer}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UpdateCustomer"
        component={UpdateCustomer}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Debt"
        component={Debt}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};
export default Navigator;
