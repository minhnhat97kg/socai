import React from "react";
import { View, TouchableOpacity, FlatList, TouchableScale } from "react-native";
import { Text, ListItem, Avatar } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./style";
import { Divider } from "react-native-elements/dist/divider/Divider";

const Item = ({ icon = "home", title = "title" }) => (
  <TouchableOpacity
    style={{
      flex: 1,
      flexDirection: "row",
      marginHorizontal: 15,
      padding: 10,
    }}
  >
    <View style={{ flex: 0.1 }}>
      <Icon name={icon} size={20} color={"black"} />
    </View>
    <View style={{ flex: 0.9 }}>
      <Text>{title}</Text>
    </View>
  </TouchableOpacity>
);

function SupplierItem({ item }) {
  const [expanded, setExpanded] = React.useState(false);

  return (
    <ListItem.Accordion
      containerStyle={styles.ListItemContainer}
      content={
        <>
          <ListItem.Content>
            <ListItem.Title>{item.name}</ListItem.Title>
            <ListItem.Subtitle>{item.phone}</ListItem.Subtitle>
          </ListItem.Content>
        </>
      }
      isExpanded={expanded}
      onPress={() => {
        setExpanded(!expanded);
      }}
    >
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 10,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
          backgroundColor: "#f7f7f7",
          borderWidth: 1,
          borderColor: "#e8e8e8",
        }}
      >
        <Item icon="search" title="Thông tin" />
        <Item icon="edit" title="Chỉnh sửa" />
        <Divider />
        <Item icon="trash" title="Xoá" />
      </View>
    </ListItem.Accordion>
  );
}

export default SupplierItem;
