import React from "react";
import { View, TouchableOpacity, FlatList } from "react-native";
import { Text, ListItem, Avatar, FAB } from "react-native-elements";
import Header from "../../components/Header";
import Container from "../../components/Container";
import SupplierItem from "./SupplierItem";
import Icon from "react-native-vector-icons/FontAwesome5";
import SearchBar from "../../components/SearchBar";
import styles from "./style";
import constants from "../../constants";

const list = [
  {
    name: "Supplier 1",
    phone: "0817288128",
  },
  {
    name: "Chris Jackson",
    phone: "0817288128",
  },
];
function SupplierScreen(props) {
  return (
    <Container>
      <Header
        leftComponent={
          <Icon
            name="chevron-left"
            size={20}
            onPress={() => props.navigation.goBack()}
          />
        }
        title={"Sổ nợ"}
      ></Header>

      <SearchBar />
      {/* Content */}
      <FlatList
        data={list}
        renderItem={({ item }) => <SupplierItem item={item} />}
        keyExtractor={(item, index) => index.toString()}
      />

      {/* Float button */}
      <FAB
        icon={<Icon name="plus" size={20} color={"white"} />}
        placement={"right"}
        color={constants.colors.main}
        onPress={() => props.navigation.navigate("CreateVendor")}
      />
    </Container>
  );
}
export default SupplierScreen;
