import { StyleSheet } from "react-native";
import constant from "../../constants";

export default StyleSheet.create({
	ListItemContainer: {
		backgroundColor: 'white',
		marginVertical: 3,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
	},
	ListItemTitle: {
		fontSize: 14,
	}
});
