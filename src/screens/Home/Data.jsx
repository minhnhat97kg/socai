import constant from "../../constants";
export default [
  {
    label: "Hoá đơn",
    icon: constant.icons.SHOPPING,
    to: "",
  },
  {
    label: "Thống kê",
    icon: constant.icons.ANALYTICS,
    to: "",
  },
  {
    label: "Khách hàng",
    icon: constant.icons.CUSTOMER,
    to: "Customer",
  },
  {
    label: "Kho",
    icon: constant.icons.BOX,
    to: "",
  },
  {
    label: "Sổ nợ",
    icon: constant.icons.BOOK,
    to: "Debt",
  },
  {
    label: "Đại lý",
    icon: constant.icons.WAREHOUSE,
    to: "Vendor",
  },
];
