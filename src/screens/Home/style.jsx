import { StyleSheet } from "react-native";
import constant from "../../constants";

export default StyleSheet.create({
  ButtonContainer: {
    flex: 1,
    flexDirection: "row",
    borderRadius: constant.style.RADIUS,
    marginHorizontal: 25,
    marginVertical: 7,
    padding: 10,
    backgroundColor: "white",
    borderColor: constant.colors.sub,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.22,
  },
  FooterContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    position: "absolute",
    height: 50,
    width: constant.window.width,
    bottom: 0,
    backgroundColor: "white",
    zIndex: 2,
  },
  Icon: {
    width: 25,
    height: 25,
  },
  ButtonLabel: {
    fontFamily: "Open Sans Bold",
    fontSize: 16,
    color: "grey",
    textAlign: "center",
  },

  Header: {
    height: 120,
    marginBottom: 50,
    padding: 10,
    backgroundColor: constant.colors.main,
  },
  Text: {
    color: "white",
    fontSize: 16,
    marginBottom: 2,
  },
  IncomeContainer: {
    position: "absolute",
    height: 100,
    width: constant.window.width * 0.9,
    bottom: -50,
    backgroundColor: "white",
    padding: 10,
    // borderRadius: constant.style.RADIUS,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    alignSelf: "center",
    shadowOpacity: 0.25,
    shadowRadius: 2.22,
    zIndex: 4,
    elevation: 4,
  },
  Money: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    color: constant.colors.main,
    marginVertical: 5,
  },
  TouchableTitle: {
    textAlign: "center",
    color: constant.colors.main,
    textDecorationLine: "underline",
  },
});
