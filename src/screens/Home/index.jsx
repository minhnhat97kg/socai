import React from "react";
import { Text, View, TouchableOpacity, Image, ScrollView } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import Container from "../../components/Container";
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./style";
import { Alert } from "react-native";
import constant from "../../constants";
import features from "./Data";

const ButtonCard = ({ title, source, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.ButtonContainer}>
        <View style={{ flex: 0.1 }}>
          <Image source={source} style={styles.Icon} />
        </View>

        <View style={{ flex: 0.9 }}>
          <Text style={styles.ButtonLabel}>{title}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Home = (props) => {
  return (
    <Container>
      {/* Header */}
      <View style={styles.Header}>
        <Text style={styles.Text}>Thu nhập so với hôm qua</Text>
        <Text style={[styles.Text, { fontWeight: "bold" }]}>
          <Icon name={"caret-up"} size={16} color={"#00FF85"} />
          {" Lời 1.5 triệu"}
        </Text>
        <View style={styles.IncomeContainer}>
          <Text style={{ fontSize: 15, color: "black" }}>Thu nhập hôm nay</Text>

          <Text style={styles.Money}>100.000.000 VND</Text>
          <TouchableOpacity>
            <Text style={styles.TouchableTitle}>Xem chi tiết lịch sử</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* Navigtion button group */}
      <ScrollView>
        <View
          style={{
            flex: 1,
          }}
        >
          {features.map((v, index) => (
            <ButtonCard
              key={index}
              title={v.label}
              source={v.icon}
              onPress={() => {
                props.navigation.navigate(v.to);
              }}
            />
          ))}
        </View>
      </ScrollView>

      {/* <LinearGradient
        // Background Linear Gradient
        colors={["rgba(255,255,255,0.4)", "transparent"]}
        start={{ x: 0, y: 1 }}
        end={{ x: 0, y: 0 }}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          bottom: -40,
          height: 80,
          zIndex: 2,
        }}
      /> */}
    </Container>
  );
};

export default Home;
