import React from "react";
import { View, TouchableOpacity, TextInput } from "react-native";
import { Text, Button } from "react-native-elements";
import Header from "../../../components/Header";
import Container from "../../../components/Container";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./style";
import constants from "../../../constants";
import Input from "../../../components/Input";
import NavigateButton from "../../../components/NavigateButton";

const user = {
    name: "Supplier 1",
    phone: "0817288128",
    address: '102 Can Tho'
}

function UpdateVendorScreen(props) {
  return (
    <Container>
      <Header
        leftComponent={
          <Icon
            name="chevron-left"
            size={20}
            onPress={() => props.navigation.goBack()}
          />
        }
        title={"Sửa thông tin đại lý"}
      ></Header>
      {/* Content */}
      <View style={styles.Container}>
        <View>
          <Input title="Tên*" placeholder={user.name} defaultValue={user.name}/>
          <Input title="Địa chỉ" placeholder={user.address} defaultValue={user.address}/>
          <Input title="Số điện thoại" placeholder={user.phone} defaultValue={user.phone}/>
        </View>
        <NavigateButton
          title="Xác nhận"
          containerStyle={styles.Button}
        />
      </View>
    </Container>
  );
}
export default UpdateVendorScreen;
