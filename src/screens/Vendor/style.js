import { StyleSheet } from "react-native";
import constant from "../../constants";

export default StyleSheet.create({
	Container: {
		backgroundColor: "white",
		marginVertical: 3,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
	},
	Content: {
		marginHorizontal: 10,
		marginVertical: 10
	},	
	Title: {
		fontSize: 25,
		color: "#525F65",
		fontWeight: "bold",
		marginBottom: 5
	},
	Subtitle: {
		fontSize: 15,
		color: "#525F65",
		fontWeight: "500",
		marginBottom: 5
	},
	DetailOptionView: {
		borderTopWidth: 2,
		borderTopColor: "#E5E5E5",
		alignItems: "center",
		backgroundColor: "white"
	},
	DetailOptionText: {
		color: "#006991",
		fontWeight: "500",
		marginVertical: 5
	},
	DetailView: {
		backgroundColor: "#E5E5E5"
	},
	Button :{
		marginBottom: constant.window.height * 0.02,
		width: constant.window.width * 0.8,
	}
});
