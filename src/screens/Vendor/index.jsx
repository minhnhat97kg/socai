import React from "react";
import { View, TouchableOpacity, FlatList } from "react-native";
import { Text, ListItem, Avatar, FAB } from "react-native-elements";
import Header from "../../components/Header";
import Container from "../../components/Container";
import VendorItem from "./VendorItem";
import Icon from "react-native-vector-icons/FontAwesome5";
import SearchBar from "../../components/SearchBar";
import styles from "./style";
import constants from "../../constants";
import NavigateButton from "../../components/NavigateButton";

const list = [
  {
    name: "Supplier 1",
    phone: "0817288128",
    address: '102 Can Tho'
  },
  {
    name: "Chris Jackson",
    phone: "0817288128",
    address: '450 new York'
  },
];
function VendorScreen(props) {
  return (
    <Container>
      <Header
        leftComponent={
          <Icon
            name="chevron-left"
            size={20}
            onPress={() => props.navigation.goBack()}
          />
        }
        rightComponent={
          <Icon name="cog" size={20} onPress={() => Alert.alert("Hello")} />
        }
      ></Header>

      <SearchBar />
      {/* Content */}
      <FlatList
        data={list}
        renderItem={({ item }) => <VendorItem item={item} navigation={props.navigation}/>}
        keyExtractor={(item, index) => index.toString()}
      />

      <NavigateButton
        title='Thêm đại lý'
        containerStyle={styles.Button}
        onPress={() => props.navigation.navigate("CreateVendor")}
      />
    </Container>
  );
}
export default VendorScreen;
