import React from "react";
import { View, TouchableOpacity, FlatList, TouchableScale, StyleSheet, Touchable } from "react-native";
import { Text } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./style";
import { Divider } from "react-native-elements/dist/divider/Divider";

const Item = ({ icon = "home", title = "title", navigation }) => (
  <TouchableOpacity
    onPress={() => navigation?.navigate("UpdateCustomer")}
    style={{
      flex: 1,
      flexDirection: "row",
      marginHorizontal: 15,
      padding: 10,
    }}
  >
    <View style={{ flex: 0.1 }}>
      <Icon name={icon} size={20} color={"black"} />
    </View>
    <View style={{ flex: 0.9 }}>
      <Text>{title}</Text>
    </View>
  </TouchableOpacity>
);

function VendorItem({ item, navigation }) {
  const [expanded, setExpanded] = React.useState(false);

  return (
    <TouchableOpacity style={styles.Container} onPress={() => setExpanded(!expanded)}>
      <View style={styles.Content}>
        <Text style={styles.Title}>{item.name}</Text>
        <Text style={styles.Subtitle}>{item.phone}</Text>
        <Text style={styles.Subtitle}>{item.address}</Text>
      </View>
      {expanded && 
        <View style={styles.DetailView}>
          <Item icon="history" title="Lịch sử mua hàng" />
          <Item icon="edit" title="Chỉnh sửa"  navigation={navigation}/>
          <Divider />
          <Item icon="trash" title="Xoá" />
        </View>
      }
      <View style={styles.DetailOptionView}>
        <Text style={styles.DetailOptionText}>{expanded? "Ẩn chi tiết" : "Chi tiết"}</Text>
      </View>
    </TouchableOpacity>
  );
}

export default VendorItem;
