import { StyleSheet } from "react-native";
import constant from "../../../constants";

export default StyleSheet.create({
	Input: {
		height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
	},
	Container:{
		flex:1,
		flexDirection:'column',
		justifyContent:'space-between',
	},
	Button :{
		marginBottom: constant.window.height * 0.02,
		width: constant.window.width * 0.8,
	}
});
