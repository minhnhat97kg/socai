import React from "react";
import { View, TouchableOpacity, TextInput } from "react-native";
import { Text, Button } from "react-native-elements";
import Header from "../../../components/Header";
import Container from "../../../components/Container";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./style";
import constants from "../../../constants";
import Input from "../../../components/Input";
import NavigateButton from "../../../components/NavigateButton";

const list = [
  {
    name: "Supplier 1",
    phone: "0817288128",
  },
  {
    name: "Chris Jackson",
    phone: "0817288128",
  },
];
function CreateCustomerScreen(props) {
  return (
    <Container>
      <Header
        leftComponent={
          <Icon
            name="chevron-left"
            size={20}
            onPress={() => props.navigation.goBack()}
          />
        }
        title={"Thêm khách hàng"}
      ></Header>
      {/* Content */}
      <View style={styles.Container}>
        <View>
          <Input title="Tên*" placeholder="Nhập tên khách hàng" />
          <Input title="Địa chỉ" placeholder="Nhập địa chỉ khách hàng" />
          <Input
            title="Số điện thoại"
            placeholder="Nhập số điện thoại khách hàng"
          />
        </View>
        <NavigateButton
          title="Xác nhận"
          containerStyle={styles.Button}
        />
      </View>
    </Container>
  );
}
export default CreateCustomerScreen;
