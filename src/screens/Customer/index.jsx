import React from "react";
import { View, TouchableOpacity, FlatList } from "react-native";
import { Text, ListItem, Avatar, FAB } from "react-native-elements";
import Header from "../../components/Header";
import Container from "../../components/Container";
import CustomerItem from "./CustomerItem";
import Icon from "react-native-vector-icons/FontAwesome5";
import SearchBar from "../../components/SearchBar";
import styles from "./style";
import constants from "../../constants";
import NavigateButton from "../../components/NavigateButton";

const list = [
  {
    name: "Customer 1",
    phone: "0817288128",
    address: '102 Can Tho'
  },
  {
    name: "Chris Jackson",
    phone: "0817288128",
    address: '450 new York'
  },
];
function CustomerScreen(props) {
  return (
    <Container>
      <Header
        leftComponent={
          <Icon
            name="chevron-left"
            size={20}
            onPress={() => props.navigation.goBack()}
          />
        }
        rightComponent={
          <Icon name="cog" size={20} onPress={() => Alert.alert("Hello")} />
        }
      ></Header>

      <SearchBar />
      {/* Content */}
      <FlatList
        data={list}
        renderItem={({ item }) => <CustomerItem item={item} navigation={props.navigation}/>}
        keyExtractor={(item, index) => index.toString()}
      />

      <NavigateButton
        title='Thêm khách hàng'
        containerStyle={styles.Button}
        onPress={() => props.navigation.navigate("CreateCustomer")}
      />
    </Container>
  );
}
export default CustomerScreen;
