import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Navigator from './src/navigations';
import { useFonts } from 'expo-font';

export default function App() {
  const [loaded] = useFonts({
    "Open Sans Bold": require('./assets/fonts/OpenSans-Bold.ttf'),
    "Open Sans Regular": require('./assets/fonts/OpenSans-Regular.ttf'),
  });

  if (!loaded) {
    return null;
  }
  return (
    <NavigationContainer>
      <Navigator />
      <StatusBar style="auto" />
    </NavigationContainer>
  );
}
